/*


list :
* Circular 
* single Linked 
* without special header node (  external pointer points the first node = head )



List code is based on the : 

Linked List Basics By Nick Parlante
Copyright © 1998-2001, Nick Parlante
cslibrary.stanford.edu/103/LinkedListBasics.pdf



http://www.martinbroadhurst.com/articles/circular-linked-list.html

Circular Linked List EENG212 Algorithms and Data Structures by Prof. Dr. Hasan AMCA
http://faraday.ee.emu.edu.tr/eeng212/

gcc c.c  -lmpc -lmpfr -lgmp -lm -Wall
gcc c.c  -lmpc -lmpfr -lgmp -Wall -g -O0

./a.out

valgrind --tool=memcheck ./a.out

valgrind --leak-check=full ./a.out




*/

#include<stdio.h>
#include<stdlib.h>
#include <math.h>
#include <complex.h>
#include <gmp.h>
#include <mpfr.h>
#include <mpc.h>





// things one can change :
#define period 34  // period of circular list = period of parabolic point
// angles in turns = rational numbers 
unsigned long long int num =  4985538889;
unsigned long long int den = 17179869183; // = (2^period) -1 
mpq_t e_angle; // external angle = num/den on parameter plane */
mpq_t i_angle; // internal angle  on parameter plane  



double er = 65536.0; // external radius  = bailout value 





static const double pi = 3.141592653589793;
static const double twopi = 6.283185307179586;




// complex variables 
mpc_t c; // parameter of complex quadratic polynomial fc(z) = z^2 + c
mpc_t z; // variable  of complex quadratic polynomial fc(z) = z^2 + c

    


// arbitrary precision library  settings
int base=10;
mp_prec_t mp_prec=200;
mpc_rnd_t rnd = MPC_RNDNN;
size_t size; // The number of significant digits, at least 2, is given by n
int inex; // return value of the function ( not the result of computation )

 FILE *files[period];



/* ---------------  structure ----------*/   


// The structure definition of the circular linked lists and the linear linked list is the same
struct node_t {
         mpc_t data ; 
         struct node_t *next ; // pointer 
 };

 struct node_t  *list=NULL;





/* -------------------- functions -------------------------------- */

// https://gitorious.org/mandelbrot/mandelbrot-numerics/source/3d55cfc99cc97decb0ba0d9c2fb271a504b8e504:c/lib/m_d_exray_in.c#L22
void Give_z0( mpc_t z0, mpq_t mpq_angle)
{

 
  double d_angle;
  complex double d_z;

  d_angle = twopi * mpq_get_d(mpq_angle);
  d_z = er * (cos(d_angle) + I * sin(d_angle));
  mpc_init2(z0, mp_prec);
  mpc_set_d_d( z0, creal(d_z), cimag(d_z), rnd);
  //mpc_set_d_d( z0, 0.0, 0.0, rnd);
   

}

// doubling map 
void mpq_doubling(mpq_t rop, const mpq_t op )
{
  mpz_t numerator;
  mpz_t denominator;
  mpz_inits(numerator, denominator, NULL);
 
  int result;
 
  mpq_add ( rop, op, op); // sum = rop = op + op 
  // 
  mpq_get_num (numerator, rop); // 
  mpq_get_den (denominator, rop); 
 // 
 result =  mpq_cmp_ui (rop , 1, 1);
 if (result>0) {
   // modulo 1 ( turn ) 
   mpz_sub( numerator, numerator,denominator);
   mpq_set_num (rop, numerator); // 
   }
 
 
 
  mpz_clears(numerator, denominator, NULL);
 
 
}
 


void PrintNode(struct node_t *current)
{
  

  printf (" data = ");
  mpc_out_str (stdout, base, 0, current->data, rnd); 
  printf (" current = %p next = %p", current,  current->next); 
  printf ("  \n");
}


void DisplayList(struct node_t *current, int length)
{

        int i=0;
	while (i<length) {
                PrintNode(current);
                current = current->next;
                i++;
              }

        
}



/* 
fc(z) =z^2+c 
inverse iteration = f^-1(z)= sqrt(z-c)
*/
void inverseiteration(mpc_t rop, mpc_t z, mpc_t c) //      
{
         
         mpc_t  mpc_temp ;
         
 
        // init and set variables 
        
        mpc_init2(mpc_temp, mp_prec); 
        mpc_sub (mpc_temp, z,  c, rnd);  // mpc_temp = z - c      
        mpc_sqrt (rop, mpc_temp, rnd);  // rop = sqrt(z-c)
 
       
         
        //
        mpc_clear(mpc_temp);
        
        

}

//Function: int mpc_sub (mpc_t rop, mpc_t op1, mpc_t op2, mpc_rnd_t rnd)





/*

build list :
* Circular 
* single Linked 
* without special header node (  external pointer points the first node = head )

*/



struct node_t * BuildList(unsigned int length) {
  struct node_t *head = NULL;
  struct node_t *newNode =NULL;
  struct node_t *tail = NULL;

  unsigned int l=0;
  mpc_t z0;
  mpc_init2(z0, mp_prec);
  

   // create single linked linear list starting from tail
  
 
  //gmp_printf (" %s %Qd \n", "e_angle = ", e_angle); //
  Give_z0( z0, e_angle);
  mpc_out_str (files[0], base, size, z0, rnd); fprintf( files[0], "\n");// save to the file 
  tail = malloc(sizeof(struct node_t));
  mpc_init2(tail->data, mp_prec);
  mpc_set(tail->data, z0, rnd) ; // newNode->data =  z 
  tail->next = NULL;
  // 
  head= tail; // list = only one node 
 

 
 for (l=1; l<length; l++) {
       mpq_doubling(e_angle, e_angle );  // angle = (2* angle ) mod 1
      //  gmp_printf (" %s %Qd \n", "e_angle = ", e_angle); //
       Give_z0( z0, e_angle);
       mpc_out_str (files[l], base, size, z0, rnd); fprintf( files[l], "\n"); // save to the file 
       newNode = malloc(sizeof(struct node_t));
       mpc_init2(newNode->data, mp_prec);
       mpc_set(newNode->data, z0, rnd) ; // newNode->data=  data; 
       newNode->next = head;
       head = newNode;
     
       
     }



   tail->next =head ; // close the circle = now list is circular list 

  mpc_clear(z0);
  return(head); // use always head pointer
}


/* 
 inverse iteration : 
 https://commons.wikimedia.org/wiki/File:Backward_Iteration.svg
 Backward iteration of complex quadratic polynomial with proper chose of the preimage
*/
void FillList(struct node_t *node, unsigned long long int imax, int listlength)
{
  unsigned long long int i; // iteration
  int l; // number of ray 

  mpc_t zn, zprev;
  mpc_init2(zn, mp_prec);
  mpc_init2(zprev, mp_prec);

   for (i=0; i<imax; i++) {
     
    for (l=0; l<listlength; l++) {
      mpc_set(zn, node->data, rnd); // read
      inverseiteration(zprev, zn, c );
      mpc_out_str (files[l], base, size, zprev, rnd); fprintf( files[l], "\n");// save to the file  
      mpc_set(node->data, zprev, rnd) ; // save zprev to the list 
      node = node->next; // next node


     }


}

 mpc_clear(zn);
 mpc_clear(zprev);
}



 // deletes all the nodes and sets list to NULL   
void FreeList(struct node_t *list, int length)
{
   struct node_t  *current=list;
 
                                         
	while (current != NULL) {
                
                //mpc_out_str (stdout, base, 0, list->data, MPC_RNDNN); printf ("  \n");
                current = current->next;
                mpc_clear(current->data);
                free(current); current=NULL; 
                
                                
              }

      
       
       
                   
}

// input : 
 // output : q rational number 
void Give_mpq(mpq_t q, unsigned long long int n, unsigned long long int d ) // q = num/dem     
{
         
         mpz_t  mpz_n ;
         mpz_t  mpz_d ;
 
        // init and set variables 
        
        mpz_init_set_ui(mpz_n,n);
        mpz_init_set_ui(mpz_d,d);
 
        mpq_init (q); // Initialize q and set it to 0/1.
        mpq_set_num(q,mpz_n);    
        mpq_set_den(q,mpz_d);
        mpq_canonicalize (q); // It is the responsibility of the user to canonicalize the assigned variable before any arithmetic operations are performed on that variable. 
         
        //
        mpz_clears(mpz_n, mpz_d, NULL);
        
        

}


void print_mpq(mpq_t q, const char *s)
{


gmp_printf (" %s %Qd \n", s, q); //  


}



/* find c in component of Mandelbrot set 
 uses complex type so #include <complex.h> and -lm 
 uses code by Wolf Jung from program Mandel
 see function mndlbrot::bifurcate from mandelbrot.cpp
 http://www.mndynamics.com/indexp.html
 
  */
// output : c 
void GiveC( mpc_t c, mpq_t mpq_InternalAngleInTurns, double InternalRadius, unsigned int iPeriod)
{

  //0 <= InternalRay<= 1
  //0 <= InternalAngleInTurns <=1
  double d_InternalAngleInTurns = mpq_get_d (mpq_InternalAngleInTurns); // conversion from mpq_t to double 
  double t = d_InternalAngleInTurns * twopi; // from turns to radians
  double R2 = InternalRadius * InternalRadius;
  double Cx, Cy; /* C = Cx+Cy*i */
  switch ( iPeriod ) {
    case 1: // main cardioid
      Cx = (cos(t)*InternalRadius)/2-(cos(2*t)*R2)/4; 
      Cy = (sin(t)*InternalRadius)/2-(sin(2*t)*R2)/4; 
      break;
   case 2: // only one component 
      Cx = InternalRadius * 0.25*cos(t) - 1.0;
      Cy = InternalRadius * 0.25*sin(t); 
      break;
  // for each period  there are 2^(period-1) roots. 
  default: // safe values
      Cx = 0.0;
      Cy = 0.0; 
    break; }
  // c is global variable so here only init and set
  mpc_init2(c, mp_prec);
  inex = mpc_set_d_d (c, Cx,Cy , rnd); // create c value from 2 double values 

}


void print_mpc(mpc_t m, const char *s)
{

printf (" %s ", s);
mpc_out_str (stdout, base, 0, m, rnd);
printf ("  \n");

}

int CreateFiles()
{
 int i;

 for ( i = 0; i < period; i++)
 {
    char filename[20];
    sprintf(filename, "%d.txt", i);
    files[i] = fopen(filename, "w");

   if (files[i] == NULL) {
       fprintf(stderr, "Can't open created file %s!\n", filename);
        return 1; }
    
      }

 return 0;
}




int setup()
{

 size = mpfr_custom_get_size(mp_prec );
 Give_mpq(i_angle, 13,   period);   print_mpq(i_angle, "i_angle = ");
 Give_mpq(e_angle, num, den);   print_mpq(e_angle, "e_angle = ");
 GiveC(c, i_angle, 1.0, 1);     print_mpc(c, "c = ");
 CreateFiles();
 
 
 
 list = BuildList(period);



 return 0;
}




int ClearMemory()
{
 int i;
 mpq_clear(e_angle); 
 mpq_clear(i_angle); 
 mpc_clear(c);
 
 
 
 FreeList(list, period );
 // close files 
 for ( i = 0; i < period; i++)
  fclose(files[i]); 

 return 0;

}



/* ---------------------------------- */

int main()

{
 
 

 
setup();

FillList(list, 100, period);
DisplayList(list, period);

ClearMemory();



return 0; 
}
